# masterclass-vs-code-server

This is a Vs-Code-Server Environment with the necessary packages and Extension installed for the masterclass EV-Charging project. The EV-Charging project can be found under projects/masterclass-main.

## Getting started

To run it docker compose is needed. The installer and installation instructions can be found [here](https://docs.docker.com/desktop/windows/install/). Once Docker is installed and started it can be run. For that, navigate via terminal into the folder where the masterclass project is saved locally. And run the docker build command.
```
cd masterclass-project-location
docker-compose up -d --build
```
Now a Docker Container should be started in Docker:
![image](https://gitlab.com/fhs36393/masterclass-vs-code-server/-/raw/main/images/docker.PNG)

The Vs-Code-Environment can be accessed via localhost:20000. How to change the port and add Extension etc. is shown in the next paragraphs.

## Function
In this project both a Dockerfile as well as a docker-compose.yml are defined. The difference is as follows.
A Dockerfile is a simple text file that contains the commands a user could call to assemble an image whereas Docker Compose is a tool for defining and running multi-container Docker applications. Docker Compose defines the services that make up your app in docker-compose.yml so they can be run together in an isolated environment. Docker compose uses the Dockerfile if you add the build command to your project’s docker-compose.yml.

## Docker-compose.yml

```yml
version: "3"
services:
  code:
    build:
      context: .
      dockerfile: Dockerfile
    restart: always
    environment:
      PUID: 1000
      PGID: 1000
      PASSWORD: password
    ports:
      - 20000:8080
    volumes:
      - ./projects:/home/coder/project
      - ./code-server:/home/coder/.local/share/code-server
```
Under environment the password for accessing the environment on the localhost can be determined. Using the PUID and PGID allows our containers to map the container's internal user to a user on the host machine. Lastly using volumes is a way for persisting data. A Volume is a folder which is shared between the container and the host-computer. It can be declared with the following syntax:
```
named_volume_name:/container/path
```
## Dockerfile

In the Dockerfile an already existing image is used as base. This is declared with the FROM Keywoard. In this project a vs code-server image from [dockerhub](https://hub.docker.com/r/codercom/code-server) was used. This already gives the basic functionalitys of vs code-server. With the RUN command basically all terminal commands can be executed. So all packages and extensions can be installed that way.


```dockerfile
FROM codercom/code-server:latest

USER root

# Install wget and other module
RUN apt-get update && apt-get install -y \
	wget

# Install code-server extensions
RUN code-server --install-extension ms-python.python
# Install code-server packages
RUN sudo apt update 
RUN sudo apt install python3-pip -y
RUN pip install ipykernel
RUN pip install pytz
RUN pip install numpy
RUN pip install mosaik
RUN pip install matplotlib
RUN pip install mosaik-pypower
RUN pip install mosaik-householdsim
RUN pip install mosaik-csv
	
# Install .NET5
ENV DOTNET_ROOT=/usr/share/dotnet
ENV PATH=/usr/share/dotnet:/root/.dotnet/tools:$PATH
#The following environment variables need to be changed
#Details 1: https://github.com/dotnet/core/issues/2186
#Details 2: https://github.com/dotnet/core/blob/master/Documentation/build-and-install-rhel6-prerequisites.md
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true

USER coder
```





