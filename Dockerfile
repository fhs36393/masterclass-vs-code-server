FROM codercom/code-server:latest

USER root

# Install wget and other module
RUN apt-get update && apt-get install -y \
	wget

# Install code-server plugin
RUN code-server --install-extension ms-python.python
RUN sudo apt update 
RUN sudo apt install python3-pip -y
RUN pip install ipykernel
RUN pip install pytz
RUN pip install numpy
RUN pip install mosaik
RUN pip install matplotlib
RUN pip install mosaik-pypower
RUN pip install mosaik-householdsim
RUN pip install mosaik-csv



	
# Install .NET5
ENV DOTNET_ROOT=/usr/share/dotnet
ENV PATH=/usr/share/dotnet:/root/.dotnet/tools:$PATH
#The following environment variables need to be changed
#Details 1: https://github.com/dotnet/core/issues/2186
#Details 2: https://github.com/dotnet/core/blob/master/Documentation/build-and-install-rhel6-prerequisites.md
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true




USER coder