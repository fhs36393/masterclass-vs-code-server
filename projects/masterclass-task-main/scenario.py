from datetime import datetime, timedelta
import pytz

import mosaik
from mosaik.util import connect_randomly, connect_many_to_one

from mosaik_pypower_grid import MosaikPyPowerGrid


SIM_CONFIG = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'ElectricVehicleSim': {
        'python': 'simulators.electric_vehicle_sim:ElectricVehicleSim',
    },
    'Collector': {
        'python': "simulators.graphic_collector:GraphicCollector"
    },
    'PyPower': {
        'python': 'mosaik_pypower.mosaik:PyPower',
    },
    'HouseholdSim': {
        'python': 'householdsim.mosaik:HouseholdSim',
    },
}

def connect_buildings_to_grid(world, houses, grid):
    buses = filter(lambda e: e.type == 'PQBus', grid.grid_elements)
    buses = {b.eid.split('-')[1]: b for b in buses}
    house_data = world.get_data(houses, 'node_id')
    for house in houses:
        node_id = house_data[house]['node_id']
        world.connect(house, buses[node_id], ('P_out', 'P'))

start_time = datetime(year=2014, month=5, day=31, hour=11, tzinfo=pytz.UTC)
start_time_string = start_time.strftime("%Y-%m-%d %H:%M:%S")

duration = timedelta(hours=3)

world = mosaik.World(SIM_CONFIG)

electric_vehicle_sim = world.start('ElectricVehicleSim')
collector_sim = world.start('Collector')
grid_sim = world.start('PyPower', step_size=60)
hhsim = world.start('HouseholdSim')
pv10kwsim = world.start('CSV', sim_start=start_time_string, datafile='./time_series/pv_10kw.csv')
pv30kwsim = world.start('CSV', sim_start=start_time_string, datafile='./time_series/pv_30kw.csv')

evs = [electric_vehicle_sim.ElectricVehicle(initial_charge=20e3, capacity=80e3, max_charging_power=11e3) for i in range(5)];

pvs10kw = pv10kwsim.PV.create(9)
pvs30kw = pv30kwsim.PV.create(3)
houses = hhsim.ResidentialLoads(sim_start=start_time_string, profile_file="time_series/profiles.data.gz", grid_name="demo_lv_grid").children

monitor = collector_sim.Monitor()
grid = MosaikPyPowerGrid(grid_sim=grid_sim, grid_file="grids/example_grid.json")
tr_pri = grid["tr_pri"]

connect_randomly(world, pvs10kw, grid.pq_busses, 'P')
connect_randomly(world, pvs30kw, grid.pq_busses, 'P')
connect_randomly(world, evs, grid.pq_busses, ('actual_charging_power', 'P'))
for ev in evs:
    world.connect(tr_pri, ev, ('P', 'transformer_power'), time_shifted=True, initial_data={'P': 0.0})

connect_buildings_to_grid(world, houses, grid)

world.connect(pvs10kw[0], monitor, 'P')
world.connect(tr_pri, monitor, 'P')

world.run(until=duration.total_seconds())