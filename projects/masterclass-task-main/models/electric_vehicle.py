class ElectricVehicle:
    def __init__(self, initial_charge, capacity, max_charging_power):
        self.charge = initial_charge
        self.capacity = capacity
        self.max_charging_power = max_charging_power
        self.actual_charging_power = 0
        self.transformer_power = 0

    def step(self, time_delta):
        self.set_charging_power()
        self.charge_vehicle(time_delta)

    def set_charging_power(self):
        self.actual_charging_power = self.max_charging_power
        
    def charge_vehicle(self, time_delta):
        self.charge += self.actual_charging_power * time_delta