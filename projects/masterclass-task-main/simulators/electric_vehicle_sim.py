import mosaik_api

from models.electric_vehicle import ElectricVehicle

META = {
    'type': 'time-based',
    'models': {
        'ElectricVehicle': {
            'public': True,
            'params': ['initial_charge', 'capacity', 'max_charging_power'],
            'attrs': ['actual_charging_power', 'transformer_power'],
        },
    },
}

class ElectricVehicleSim(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.eid_prefix = "EV_"
        self.vehicles = {}

    def init(self, sid, **kwargs):
        return self.meta
    
    def create(self, num, model, initial_charge, capacity, max_charging_power):
        next_eid = len(self.vehicles)
        created_vehicles = []

        for i in range(next_eid, next_eid + num):
            vehicle_instance = ElectricVehicle(initial_charge, capacity, max_charging_power)
            eid = f'{self.eid_prefix}{i}'
            self.vehicles[eid] = vehicle_instance
            created_vehicles.append({'eid': eid, 'type': model})
        
        return created_vehicles
    
    def step(self, time, inputs, max_advance):
        for eid, vehicle_instance in self.vehicles.items():
            if eid in inputs:
                attributes = inputs[eid]
                assert(len(attributes)==1)
                all_transformer_power = attributes["transformer_power"]
                assert(len(attributes)==1)
                vehicle_instance.transformer_power = list(all_transformer_power.values())[0]

            vehicle_instance.step(1)
        
        return time + 1

    def get_data(self, outputs):
        data = {}
        for eid, attrs in outputs.items():
            model = self.vehicles[eid]
            data[eid] = {}
            for attr in attrs:
                if attr in self.meta['models']['ElectricVehicle']['attrs']:
                    data[eid][attr] = getattr(model, attr)
                else:
                    raise ValueError(f"Unknown output attribute: {attr}")

        return data