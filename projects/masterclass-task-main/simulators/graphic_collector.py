import numpy as np
import matplotlib.pyplot as plt

from simulators.collector import Collector


class GraphicCollector(Collector):
    def finalize(self):
        print('Displaying collected data ...')

        for sim, sim_data in sorted(self.data.items()):
            n_attributes = len(sim_data.keys())

            fig, axes = plt.subplots(1, n_attributes)
            fig.suptitle(sim)
            axes = axes.ravel() if n_attributes > 1 else [axes]

            for idx, (attr, values) in enumerate(sorted(sim_data.items())):
                time_steps = np.array(list(values.keys()))
                simulator_values = np.array(list(values.values()))

                n_steps = len(time_steps)
                axes[idx].plot(time_steps, simulator_values, '-', color="blue")
                axes[idx].plot(time_steps, [np.mean(simulator_values)]*n_steps, '--', color="red")
                axes[idx].plot(time_steps, [np.quantile(simulator_values, 0.05)]*n_steps, '--', color="orange")
                axes[idx].plot(time_steps, [np.quantile(simulator_values, 0.95)]*n_steps, '--', color="orange")

                axes[idx].set_title(f"{attr}")
                axes[idx].set_xlabel("time [s]")
                axes[idx].set_ylabel("power [W]")
                axes[idx].set_xlim(np.min(time_steps), np.max(time_steps))

            plt.show()
