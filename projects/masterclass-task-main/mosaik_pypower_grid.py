class MosaikPyPowerGrid:
    def __init__(self, grid_sim, grid_file):
        self.grid_elements = grid_sim.Grid(gridfile=grid_file).children

        self.pq_busses = self._extract_elements_where_type("PQBus")
        self.ref_bus = self._extract_first_element_where_type("RefBus")
        self.busses = [self.ref_bus] + self.pq_busses

        self.regular_branches = self._extract_elements_where_type("Branch")
        self.transformer_branch = self._extract_first_element_where_type("Transformer")
        self.branches = [self.transformer_branch] + self.regular_branches

    def _extract_elements_where_type(self, type_name):
        return list(self._grid_elements_where_type(type_name))

    def _extract_first_element_where_type(self, type_name):
        return next(self._grid_elements_where_type(type_name))

    def _grid_elements_where_type(self, type_name):
        for element in self.grid_elements:
            if element.type == type_name:
                yield element

    def __getitem__(self, key):
        return next(element for element in self.grid_elements if key in element.eid)

    def __str__(self):
        instance_as_string = "Grid: Branches: {"
        for branch in self.branches:
            instance_as_string += f"{branch.eid}, "
        instance_as_string += "} Busses: {"
        for bus in self.busses:
            instance_as_string += f"{bus.eid}, "
        instance_as_string += "}"

        return instance_as_string
